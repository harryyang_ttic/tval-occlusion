#include <pcl/point_cloud.h>
#include <pcl/octree/octree.h>
#include <pcl/surface/texture_mapping.h>

#include <iostream>
#include <vector>
#include<fstream>
#include<sstream>

#include <eigen3/Eigen/Dense>

using namespace std;
using namespace pcl;
using namespace pcl::texture_mapping;
using namespace Eigen;

char* point_name, *vc_name, *point_out_name;
double resolution;

void ParseArguments(int argc, char** argv)
{
    if(argc<4)
    {
        cout<<"usage: point_name vc_name point_out_name resolution"<<endl;
        exit(-1);
    }
    point_name=argv[1];
    vc_name=argv[2];
    point_out_name=argv[3];
    resolution=atof(argv[4]);
}

vector<vector<double> > ReadPoints()
{
    ifstream myfile(point_name);
    string line;
    vector<vector<double> > pts;
    while(getline(myfile,line))
    {
        istringstream is(line);
        vector<double> tmp(6);
        is>>tmp[0]>>tmp[1]>>tmp[2]>>tmp[3]>>tmp[4]>>tmp[5];
        pts.push_back(tmp);
    }
    return pts;
}

inline Matrix4f ReadVCMatrix(const char* vc_name)
{
    Matrix4f M_v_c;
    std::ifstream v_c(vc_name);
    for(int i=0;i<4;i++)
    {
        for(int j=0;j<4;j++)
        {
            v_c>>M_v_c(i,j);
        }
    }
    v_c.close();
    return M_v_c;
}

vector<double> TransformPoint(vector<double> point, Matrix4f M_v_c)
{
    VectorXf X(4);
    X(0) = point[0]/100.0;
    X(1) = point[1]/100.0;
    X(2) = point[2]/100.0;
    X(3) = 1;
    VectorXf X1 = M_v_c * X;
    vector<double> point_out(3);
    point_out[0]=X1(0);
    point_out[1]=X1(1);
    point_out[2]=X1(2);

    return point_out;
}

PointCloud<PointXYZ>::Ptr ConstructPointCloud(vector<vector<double> > pts, Matrix4f M_v_c)
{
    PointCloud<PointXYZ>::Ptr cloud(new pcl::PointCloud<PointXYZ>);
    int pt_num=pts.size();
    cloud->width = pt_num;
    cloud->height = 1;
    cloud->points.resize (cloud->width * cloud->height);
    for (size_t i = 0; i < pt_num; ++i)
    {
        vector<double> pt_old=pts[i];
        vector<double> pt_new=TransformPoint(pt_old, M_v_c);
        cloud->points[i].x = pt_new[0];
        cloud->points[i].y = pt_new[1];
        cloud->points[i].z = pt_new[2];
    }
    return cloud;
}

octree::OctreePointCloudSearch<pcl::PointXYZ> ConstructOctree(PointCloud<PointXYZ>::Ptr cloud)
{
    octree::OctreePointCloudSearch<pcl::PointXYZ> octree (resolution);
    octree.setInputCloud (cloud);
    octree.addPointsFromInputCloud ();
    return octree;
}

void DetectOcclusion(PointCloud<PointXYZ>::Ptr cloud, octree::OctreePointCloudSearch<pcl::PointXYZ> octree,vector<vector<double> > pts)
{
    int pt_num=cloud->points.size();

   // octree::OctreePointCloudSearch<pcl::PointXYZ>::Ptr octreePtr;
    boost::shared_ptr<octree::OctreePointCloudSearch<pcl::PointXYZ> > octreePtr(new octree::OctreePointCloudSearch<pcl::PointXYZ>(octree));
   // octreePtr->setInputCloud(octree.PointCloud);
    ofstream point_out(point_out_name);
    int occlude_num=0;
    TextureMapping<PointXYZ> texMap;
    for(int i=0;i<pt_num;i++)
    {
        PointXYZ pt=cloud->points[i];
        bool isOccluded=texMap.isPointOccluded(pt,octreePtr);
        if(!isOccluded)
            point_out<<pts[i][0]<<" "<<pts[i][1]<<" "<<pts[i][2]<<" "<<pts[i][3]<<" "<<pts[i][4]<<" "<<pts[i][5]<<endl;
        else
            occlude_num++;
    }
    cout<<pt_num<<" "<<occlude_num<<endl;
    point_out.close();
}


int main(int argc, char** argv)
{
    ParseArguments(argc, argv);
    Matrix4f M_v_c=ReadVCMatrix(vc_name);
    vector<vector<double> > pts=ReadPoints();
    PointCloud<PointXYZ>::Ptr cloud=ConstructPointCloud(pts,M_v_c);
    octree::OctreePointCloudSearch<pcl::PointXYZ> octree=ConstructOctree(cloud);
    DetectOcclusion(cloud,octree,pts);
    return 0;
}
